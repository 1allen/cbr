<?php

namespace app\commands;

use app\models\Currency;
use app\models\CurrencyData;
use yii\console\Controller;

use yii\httpclient\Client;

/**
 * cbr stuff
 */
class CbrController extends Controller
{
    /**
     * Fetch actual rates for today, or date specified.
     * @param string $date date in (CurrencyData::DATE_FORMAT) format
     */
    public function actionIndex($date = null)
    {
        $currCodes = Currency::codesMap();
        if (!$currCodes) {
            $this->error(\Yii::t('app', 'No currency codes found in db :('));
        }

        if (!$date) {
            $timeStamp = time();
        } else {
            $timeStamp = \DateTime::createFromFormat(CurrencyData::DATE_FORMAT, $date)->getTimestamp();
        }
        $dateToRequest = date('d/m/Y', $timeStamp);

        $client = new Client();
        $response = $client->get('http://www.cbr.ru/scripts/XML_daily_eng.asp',
            [
                'date_req' => $dateToRequest,
            ],
            [
                'format' => Client::FORMAT_XML,
            ])
            ->send();

        if ($response->isOk) {
            foreach ($response->data['Valute'] as $datum) {
                if ($currencyId = array_search($datum->CharCode, $currCodes)) {
                    $model = new CurrencyData();
                    $attributes = [
                        'date' => $dateToRequest,
                        'currency_id' => $currencyId,
                        'value' => floatval(str_replace(',', '.', $datum->Value)),
                    ];
                    $model->setAttributes($attributes);

                    // do nothing, if we already have this rate for this day
                    if ($dupes = $model::find()->where($attributes)->count() == 0) {
                        if (!$model->save()) {
                            $this->stderr(\Yii::t('app', 'error saving rate. errors: ') . var_export($model->errors));
                        }
                    }
                }
            }
        } else {
            $this->error(\Yii::t('app', 'response is not ok. raw body: ') . $response->content);
        }
    }

    /**
     * @param string $msg error message
     * @param bool $returnError should we return error code? default is true
     * @return int
     */
    private function error($msg, $returnError = true)
    {
        $this->stderr($msg);
        \Yii::$app->mailer->compose()
            ->setTo(\Yii::$app->params['adminEmail'])
            ->setSubject('Error occurred while downloading CBR rates')
            ->setTextBody('Text message as follows: ' . PHP_EOL . $msg)
            ->send();
        if ($returnError) {
            return Controller::EXIT_CODE_ERROR;
        }
        return Controller::EXIT_CODE_NORMAL;
    }
}
