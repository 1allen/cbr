<?php

return [
    'class' => yii\db\Connection::class,
    'dsn' => 'sqlite:' . __DIR__ . DIRECTORY_SEPARATOR
        . '..' . DIRECTORY_SEPARATOR
        . 'db.sqlite',
    'username' => 'root',
    'password' => '',
    'charset' => 'utf8',
];
