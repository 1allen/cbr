<?php

use yii\db\Migration;

class m170103_125031_initial extends Migration
{
    public function up()
    {
        $this->createTable('currencies', [
            'id' => $this->primaryKey(),
            'code' => $this->string()->notNull(),
            'name' => $this->string(),
        ]);

        $this->createTable('currency_data', [
            'id' => $this->primaryKey(),
            'date' => $this->date(),
            'currency_id' => $this->integer(),
            'value' => $this->money(),
        ]);

        if ($this->db->driverName !== 'sqlite') {
            $this->addForeignKey('data_currencies_id_fk', 'currency_data', 'currency_id', 'currencies', 'id');
        }

        $this->insert('currencies', [
            'code' => 'USD',
            'name' => 'US Dollar',
        ]);
        $this->insert('currencies', [
            'code' => 'EUR',
            'name' => 'Euro',
        ]);
        $this->insert('currencies', [
            'code' => 'GBP',
            'name' => 'British Pound',
        ]);
        $this->insert('currencies', [
            'code' => 'CAD',
            'name' => 'Canadian Dollar',
        ]);
    }

    public function down()
    {
        echo "m170103_125031_initial cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
