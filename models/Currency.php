<?php

namespace app\models;

use yii\helpers\ArrayHelper;


/**
 * This is the model class for table "currencies".
 *
 * @property integer $id
 * @property string $code
 * @property string $name
 */
class Currency extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'currencies';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['code'], 'required'],
            [['code', 'name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'code' => 'Code',
            'name' => 'Name',
        ];
    }

    /**
     * returns map of currencies we currently support. ex: [ 1 => 'USD', 2 => 'EUR' ]
     * @return array
     */
    public static function codesMap()
    {
        $currencies = self::find()->select(['id', 'code'])->all();
        return ArrayHelper::map($currencies, 'id', 'code');
    }
}
