<?php

namespace app\models;

/**
 * This is the model class for table "currency_data".
 *
 * @property integer $id
 * @property string $date
 * @property integer $currency_id
 * @property string $value
 */
class CurrencyData extends \yii\db\ActiveRecord
{

    /** default date format */
    const DATE_FORMAT = 'd/m/Y';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'currency_data';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date'], 'date', 'format' => 'php:' . self::DATE_FORMAT],
            [['currency_id'], 'integer'],
            [['value'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date' => 'Date',
            'currency_id' => 'Currency ID',
            'value' => 'Value',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCurrency()
    {
        return $this->hasOne(Currency::className(), ['id' => 'currency_id']);
    }
}
