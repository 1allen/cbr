<?php
/* @var $this yii\web\View */

use kartik\date\DatePicker;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\grid\GridView;

$this->title = 'CBR rates';
$date = Yii::$app->request->getQueryParam('date');
?>
<div class="container">
    <div class="row">
        <?php $form = ActiveForm::begin([
            'method' => 'get',
            'action' => null,
            'layout' => 'inline',
        ]); ?>

        <div class="col-lg-6 col-lg-offset-4">
            <?php echo $this->title ?> for <?php echo DatePicker::widget([
                'name' => 'date',
                'value' => $date,
                'type' => DatePicker::TYPE_COMPONENT_APPEND,
                'pluginOptions' => [
                    'autoclose' => true,
                    'format' => 'dd/mm/yyyy',
                ]
            ]); ?>
        </div>

        <div class="col-lg-1">
            <?php echo Html::submitButton(\Yii::t('app', 'Submit'), ['class' => 'btn btn-primary']); ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
    <br>
    <br>
    <div class="row">
        <div class="col-lg-12">
            <?php echo GridView::widget([
                'dataProvider' => $data,
                'columns' => [
                    [
                        'attribute' => 'date',
                    ],
                    [
                        'attribute' => 'currency.code'
                    ],
                    [
                        'attribute' => 'value',
                    ],
                ],
            ]); ?>
        </div>
    </div>
</div>
